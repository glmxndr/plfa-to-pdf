#! /bin/bash

BASE=https://plfa.github.io

#FORMAT="--page-height 210mm --page-width 157mm -B 2mm -T 10mm -L 2mm -R 30mm --zoom 0.8 "
FORMAT="--page-height 210mm --page-width 157mm -O Landscape -B 2mm -L 2mm -T 2mm -R 50mm --zoom 0.9 "
OUTLINE="--outline --outline-depth 2 "
STYLE="--user-style-sheet /app/style.css "
DEST=/app/plfa.pdf

set -x

function wkhtmltopdf () {
    docker run -v $(pwd):/app surnet/alpine-wkhtmltopdf:3.10-0.12.5-full $*
}

wkhtmltopdf $FORMAT $OUTLINE $STYLE \
    cover /app/title.html \
    cover /app/frontmatter.html \
    page $BASE/Dedication/  \
    page $BASE/Preface/ \
    page $BASE/GettingStarted/ \
    page $BASE/2019/07/12/changes-to-plfa.html \
    cover /app/part1.html \
    page $BASE/Naturals/  \
    page $BASE/Induction/  \
    page $BASE/Relations/  \
    page $BASE/Equality/  \
    page $BASE/Isomorphism/  \
    page $BASE/Connectives/  \
    page $BASE/Negation/  \
    page $BASE/Quantifiers/  \
    page $BASE/Decidable/  \
    page $BASE/Lists/ \
    cover /app/part2.html \
    page $BASE/Lambda/ \
    page $BASE/Properties/ \
    page $BASE/DeBruijn/ \
    page $BASE/More/ \
    page $BASE/Bisimulation/ \
    page $BASE/Inference/ \
    page $BASE/Untyped/ \
    cover /app/backmatter.html \
    page $BASE/Acknowledgements/ \
    page $BASE/Fonts/ \
    page $BASE/Statistics/ \
    $DEST
